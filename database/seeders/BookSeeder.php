<?php

namespace Database\Seeders;

use App\Models\Item;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataBook = [
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Publikasi Umum, informasi umum dan komputer'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Bibiliografi'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Perpustakaan dan informasi'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Ensiklopedia dan buku yang memuat fakta-fakta'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Tidak ada klasifikasi (sebelumnya untuk Biografi)'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Majalah dan Jurnal'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Asosiasi, Organisasi dan Museum'],
            ['floor' => 1, 'stock' => 100, 'name' => '[Test] Media massa, junalisme dan publikasi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Kutipan'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Manuskrip dan buku langka'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Filsafat dan psikologi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Metafisika'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Epistimologi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Parapsikologi dan Okultisme'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Pemikiran Filosofis'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Psikologi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Filosofis Logis'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Etik'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Filosofi kuno, zaman pertengahan, dan filosofi ketimuran'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Filosofi barat modern'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Agama'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Ilmu sosial, sosiologi dan antropologi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Statistik'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Ilmu politik'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Ekonomi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Hukum'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Administrasi publik dan ilmu kemiliteran'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Masalah dan layanan sosial'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Pendidikan'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Perdagangan, komunikasi dan transportasi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Norma, etika dan tradisi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Bahasa'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Sains'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Matematika'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Astronomi'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Fisika'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Kimia'],
            ['floor' => 2, 'stock' => 100, 'name' => '[Test] Ilmu kebumian dan geologi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Fosil dan kehidupan prasejarah'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Biologi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Tanaman'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Zoologi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Teknologi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Kesehatan dan Obat-Obatan'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Teknik'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Pertanian'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Managemen Rumah Tangga dan Keluarga'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Manajemen dan Hubungan dengan Publik'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Teknik Kimia'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Manufaktur'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Manufaktur untuk Keperluan Khusus'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Konstruksi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Kesenian dan rekreasi'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Perencanaan dan Arsitektur Lanskap'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Arsitektur'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Patung, Keramik dan Seni Metal'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Seni Grafis dan Dekoratif'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Lukisan'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Percetakan'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Fotografi, Film, Video'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Musik'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Olahraga, Permainan dan Hiburan'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Literatur, Sastra, Retorika dan Kritik'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Sejarah'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Geografi dan Perjalanan'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Biografi dan Asal-Usul'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Sejarah Dunia Lama'],
            ['floor' => 3, 'stock' => 100, 'name' => '[Test] Asal–Usul Eropa'],
            ['floor' => 4, 'stock' => 100, 'name' => '[Test] Asal-Usul Asia'],
            ['floor' => 5, 'stock' => 100, 'name' => '[Test] Asal-Usul Afrika'],
            ['floor' => 5, 'stock' => 100, 'name' => '[Test] Asal-Usul Amerika Utara'],
            ['floor' => 5, 'stock' => 100, 'name' => '[Test] Asal-Usul Amerika Selatan'],
            ['floor' => 5, 'stock' => 100, 'name' => '[Test] Asal-Usul Wilayah Lain'],
        ];
        for ($i = 0; $i < count($dataBook); $i++) {
            $item = Item::create($dataBook[$i]);
            $item->categories()->attach($i+1);
        }
    }
}
