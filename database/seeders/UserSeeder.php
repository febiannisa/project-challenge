<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $su = User::create([
            'name' => 'superuser',
            'email' => 'su@local',
            'is_active' => 1,
            'password' => $password = Hash::make('password'),
            'role' => 'admin',
        ]);
        
        $user = User::create([
            'name' => 'user',
            'email' => 'user@local',
            'is_active' => 1,
            'password' => $password,
            'role' => 'user',
        ]);
    }
}
