@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="card-box mb-30">
    <div class="pd-20">
      <h4 class="text-blue h4">Data Buku</h4> 
      @if(request()->user()->role == 'admin')
      <a href="/item/create" class="btn btn-primary">Tambah</a>
      @endif
    </div>
    
    <div class="pb-20">
      <table class="data-table table stripe hover nowrap">
        <thead>
          <tr>
            <th colspan="2" class="datatable-nosort"></th>
            <th colspan="3" class="datatable-nosort text-center">Stok</th>
            <th class="datatable-nosort"></th>
          </tr>
          <tr>
            <th class="table-plus">Nama Buku</th>
            <th>Kategori</th>
            <th>Total</th>
            <th>Dipinjam</th>
            <th>Tersedia</th>
            <th class="datatable-nosort">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($items as $item)
          <tr>
            <td class="table-plus">{{ $item->name }}</td>
            <td>
              @foreach ($item->categories as $category)
              <span>{{ $category->name }}</span>
              @endforeach
            </td>
            <td>{{ $item->stock }}</td>
            <td>{{ $item->borrowed_count }}</td>
            <td>{{ $item->stock - $item->borrowed_count }}</td>
            <td>
              <div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                  <i class="dw dw-more"></i>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                  <a class="dropdown-item" href="{{ route('item.show', $item->id) }}"><i class="dw dw-eye"></i> View</a>
                  @if(request()->user()->role == 'admin')
                  <a class="dropdown-item" href="{{ route('item.edit', $item->id) }}"><i class="dw dw-edit2"></i> Edit</a>
                  <form action="{{ route('item.destroy', $item->id) }}" method="post" onsubmit="event.preventDefault(); Swal.fire({
                    text: 'apakah anda yakin?',
                    icon: 'warning',
                    showCancelButton: true,
                  }).then(response => response.isConfirmed && event.target.submit())">
                    @csrf
                    @method('delete')
                    <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                  </form>
                  @endif

                  @if (request()->user()->is_active === 1)
                    <form action="{{ route('item.borrow', $item->id) }}" method="POST">
                      @csrf
                      <button type="submit" class="dropdown-item"><i class="dw dw-cart"></i> Pinjam</button>
                    </form>
                  @endif
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection