<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'item_id',
        'user_id',
        'borrowed_at',
        'borrow_approved',
        'returned_at',
        'return_approved',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
