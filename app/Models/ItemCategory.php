<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    use HasFactory;

    protected $table = 'category_item';

    /**
     * @var string[]
     */
    protected $fillable = [
        'item_id',
        'category_id',
    ];
}
