@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="card-box mb-30">
    <div class="pd-20">
      <h4 class="text-blue h4">Data User</h4>
      <a href="/user/create" class="btn btn-primary">Tambah</a>
    </div>
    <div class="pb-20">
      <table class="data-table table stripe hover nowrap">
        <thead>
          <tr>
            <th class="table-plus datatable-nosort">Nama</th>
            <th>Email</th>
            <th>Active</th>
            <th>Role</th>
            <th class="datatable-nosort">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($users as $user)
          <tr>
            <td class="table-plus">{{ mb_strtoupper($user->name) }}</td>
            <td>{{ mb_strtoupper($user->email) }}</td>
            <td>
              @if ($user->is_active === 1)
                <span class="badge bg-success">Aktif</span>
              @else
                <span class="badge bg-danger">
                  Tidak Aktif
                </span>
              @endif
            </td>
            <td>{{ mb_strtoupper($user->role) }}</td>
            <td>
              <div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                  <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                  <a class="dropdown-item" href="{{ route('user.show', $user->id) }}"><i class="dw dw-eye"></i> View</a>
                  <a class="dropdown-item" href="{{ route('user.edit', $user->id) }}"><i class="dw dw-edit2"></i> Edit</a>
                  <form action="{{ route('user.destroy', $user->id) }}" method="POST" onsubmit="event.preventDefault(); Swal.fire({
                    text: 'apakah anda yakin?',
                    icon: 'question',
                    showCancelButton: true,
                  }).then(response => response.isConfirmed && event.target.submit())">
                    @csrf
                    @method('delete')
                    <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                  </form>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection