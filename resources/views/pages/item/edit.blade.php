@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <form action="{{ route('item.update', $item->id) }}" method="POST">
    @csrf
    @method('patch')
    <div class="form-group">
      <label>Nama Buku</label>
      <input type="text" class="form-control" name="name" value="{{ $item->name }}">
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    
    <div class="form-group">
      <label>Total Stok</label>
      <input type="number" class="form-control" name="stock" value="{{ $item->stock }}">
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror

    <div class="form-group">
      <label>Kategori Buku</label>
      <select name="categories[]" class="custom-select col-12 multiple" multiple>
        @foreach($categories as $category)
        <option value="{{ $category->id }}" @if ($item->categories->find($category->id)) selected @endif>{{ mb_strtoupper($category->name) }}</option>
        @endforeach
      </select>
    </div>

    @error('categories')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection