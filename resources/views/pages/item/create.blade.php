@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <form action="{{ route('item.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Buku</label>
      <input type="text" class="form-control" name="name">                
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror

    <div class="form-group">
      <label>Lantai</label>
      <input type="text" class="form-control" name="floor">                
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror
    
    <div class="form-group">
      <label>Total Stok</label>
      <input type="number" class="form-control" name="stock">                
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror

    <div class="form-group">
      <label>Kategori Buku</label>
      <select name="categories[]" class="custom-select col-12 multiple" multiple>
        @foreach($categories as $category)
        <option value="{{ $category->id }}">{{ mb_strtoupper($category->name) }}</option>
        @endforeach
      </select>
    </div>

    @error('categories')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection