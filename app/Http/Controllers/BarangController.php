<?php

namespace App\Http\Controllers;

use App\Models\Borrow;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Buku';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.item.index')->with([
            'data' => $data,
            'items' => Item::with('categories')->get(),
        ]);
    }

    public function search()
    {
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.item.search')->with([
            'data' => $data,
            'items' => Item::with('categories')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Buku';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        
        return view('pages.item.create')->with([
            'data' => $data,
            'categories' => Category::get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Item::create($request->validate([
            'name' => 'required',
            'stock' => 'required',
            'floor' => 'required',
            'categories.*' => 'nullable|exists:categories,id',
        ]));

        $item->categories()->attach($request->categories);

        return redirect()->route('item.index')->with('success', 'Buku berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Buku';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';        

        return view('pages.item.show')->with([
            'data' => $data,
            'item' => $item,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Buku';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        
        return view('pages.item.edit')->with([
            'data' => $data,
            'item' => $item,
            'categories' => Category::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $item->update($request->validate([
            'name' => 'required',
            'stock' => 'required',
            'categories.*' => 'nullable|exists:categories,id',
        ]));

        $item->categories()->sync($request->categories);

        return redirect()->route('item.index')->with('success', 'Buku berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($item->delete()) {
            return redirect()->route('item.index')->with('success', 'Buku berhasil dihapus');
        }

        return redirect()->route('item.index')->with('error', 'Buku gagal dihapus');
    }

    /**
     * @param \App\Models\Item $item
     * @return \Illuminate\Http\Response
     */
    public function borrow(Item $item)
    {
        Borrow::create([
            'item_id' => $item->id,
            'user_id' => request()->user()->id,
            'borrowed_at' => now(),
        ]);

        return redirect()->back()->with('success', 'Buku berhasil dipinjam');
    }
}
