@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <form action="{{ route('category.store') }}" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" class="form-control" name="name">                
    </div>
    @error('name')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror
    <div class="form-group">
      <label for="exampleInputPassword1">Deskripsi Kategori</label>
      <textarea name="description" class="form-control"></textarea>
    </div>
    @error('description')
      <div class="alert alert-danger">{{$message}}</div>                
    @enderror           
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection