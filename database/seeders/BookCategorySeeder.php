<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class BookCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataBookCategory = [
            ['name' => 'Publikasi Umum, informasi umum dan komputer'],
            ['name' => 'Bibiliografi'],
            ['name' => 'Perpustakaan dan informasi'],
            ['name' => 'Ensiklopedia dan buku yang memuat fakta-fakta'],
            ['name' => 'Tidak ada klasifikasi (sebelumnya untuk Biografi)'],
            ['name' => 'Majalah dan Jurnal'],
            ['name' => 'Asosiasi, Organisasi dan Museum'],
            ['name' => 'Media massa, junalisme dan publikasi'],
            ['name' => 'Kutipan'],
            ['name' => 'Manuskrip dan buku langka'],
            ['name' => 'Filsafat dan psikologi'],
            ['name' => 'Metafisika'],
            ['name' => 'Epistimologi'],
            ['name' => 'Parapsikologi dan Okultisme'],
            ['name' => 'Pemikiran Filosofis'],
            ['name' => 'Psikologi'],
            ['name' => 'Filosofis Logis'],
            ['name' => 'Etik'],
            ['name' => 'Filosofi kuno, zaman pertengahan, dan filosofi ketimuran'],
            ['name' => 'Filosofi barat modern'],
            ['name' => 'Agama'],
            ['name' => 'Ilmu sosial, sosiologi dan antropologi'],
            ['name' => 'Statistik'],
            ['name' => 'Ilmu politik'],
            ['name' => 'Ekonomi'],
            ['name' => 'Hukum'],
            ['name' => 'Administrasi publik dan ilmu kemiliteran'],
            ['name' => 'Masalah dan layanan sosial'],
            ['name' => 'Pendidikan'],
            ['name' => 'Perdagangan, komunikasi dan transportasi'],
            ['name' => 'Norma, etika dan tradisi'],
            ['name' => 'Bahasa'],
            ['name' => 'Sains'],
            ['name' => 'Matematika'],
            ['name' => 'Astronomi'],
            ['name' => 'Fisika'],
            ['name' => 'Kimia'],
            ['name' => 'Ilmu kebumian dan geologi'],
            ['name' => 'Fosil dan kehidupan prasejarah'],
            ['name' => 'Biologi'],
            ['name' => 'Tanaman'],
            ['name' => 'Zoologi'],
            ['name' => 'Teknologi'],
            ['name' => 'Kesehatan dan Obat-Obatan'],
            ['name' => 'Teknik'],
            ['name' => 'Pertanian'],
            ['name' => 'Managemen Rumah Tangga dan Keluarga'],
            ['name' => 'Manajemen dan Hubungan dengan Publik'],
            ['name' => 'Teknik Kimia'],
            ['name' => 'Manufaktur'],
            ['name' => 'Manufaktur untuk Keperluan Khusus'],
            ['name' => 'Konstruksi'],
            ['name' => 'Kesenian dan rekreasi'],
            ['name' => 'Perencanaan dan Arsitektur Lanskap'],
            ['name' => 'Arsitektur'],
            ['name' => 'Patung, Keramik dan Seni Metal'],
            ['name' => 'Seni Grafis dan Dekoratif'],
            ['name' => 'Lukisan'],
            ['name' => 'Percetakan'],
            ['name' => 'Fotografi, Film, Video'],
            ['name' => 'Musik'],
            ['name' => 'Olahraga, Permainan dan Hiburan'],
            ['name' => 'Literatur, Sastra, Retorika dan Kritik'],
            ['name' => 'Sejarah'],
            ['name' => 'Geografi dan Perjalanan'],
            ['name' => 'Biografi dan Asal-Usul'],
            ['name' => 'Sejarah Dunia Lama'],
            ['name' => 'Asal–Usul Eropa'],
            ['name' => 'Asal-Usul Asia'],
            ['name' => 'Asal-Usul Afrika'],
            ['name' => 'Asal-Usul Amerika Utara'],
            ['name' => 'Asal-Usul Amerika Selatan'],
            ['name' => 'Asal-Usul Wilayah Lain'],
        ];
        for ($i = 0; $i < count($dataBookCategory); $i++) {
            Category::create($dataBookCategory[$i]);
        }
    }
}
