<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>@yield('title')</title>

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('deskapp2-master/vendors/images/apple-touch-icon.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('deskapp2-master/vendors/images/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('deskapp2-master/vendors/images/favicon-16x16.png')}}">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/core.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/src/plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/src/plugins/datatables/css/responsive.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('deskapp2-master/vendors/styles/style.css')}}">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script src="{{ asset('/js/sweetalert2.min.js') }}" defer></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000
			});

			@if(session('success'))
				Toast.fire({
					icon: 'success',
					title: '{{ session('success') }}'
				})
			@endif

			@if(session('error'))
				Toast.fire({
					icon: 'error',
					title: '{{ session('error') }}'
				})
			@endif

			$('.multiple').select2();
		});
	</script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>
<body>
	<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="{{asset('deskapp2-master/vendors/images/deskapp-logo.svg')}}" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div>


	<div class="right-sidebar">
		<div class="sidebar-title">
			<h3 class="weight-600 font-16 text-blue">
				Layout Settings
				<span class="btn-block font-weight-400 font-12">User Interface Settings</span>
			</h3>
			<div class="close-sidebar" data-toggle="right-sidebar-close">
				<i class="icon-copy ion-close-round"></i>
			</div>
		</div>
		<div class="right-sidebar-body customscroll">
			<div class="right-sidebar-body-content">
				<h4 class="weight-600 font-18 pb-10">Header Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary header-white active">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary header-dark">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Sidebar Background</h4>
				<div class="sidebar-btn-group pb-30 mb-10">
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-light ">White</a>
					<a href="javascript:void(0);" class="btn btn-outline-primary sidebar-dark active">Dark</a>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu Dropdown Icon</h4>
				<div class="sidebar-radio-group pb-10 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-1" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-1" checked="">
						<label class="custom-control-label" for="sidebaricon-1"><i class="fa fa-angle-down"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-2" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-2">
						<label class="custom-control-label" for="sidebaricon-2"><i class="ion-plus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebaricon-3" name="menu-dropdown-icon" class="custom-control-input" value="icon-style-3">
						<label class="custom-control-label" for="sidebaricon-3"><i class="fa fa-angle-double-right"></i></label>
					</div>
				</div>

				<h4 class="weight-600 font-18 pb-10">Menu List Icon</h4>
				<div class="sidebar-radio-group pb-30 mb-10">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-1" name="menu-list-icon" class="custom-control-input" value="icon-list-style-1" checked="">
						<label class="custom-control-label" for="sidebariconlist-1"><i class="ion-minus-round"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-2" name="menu-list-icon" class="custom-control-input" value="icon-list-style-2">
						<label class="custom-control-label" for="sidebariconlist-2"><i class="fa fa-circle-o" aria-hidden="true"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-3" name="menu-list-icon" class="custom-control-input" value="icon-list-style-3">
						<label class="custom-control-label" for="sidebariconlist-3"><i class="dw dw-check"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-4" name="menu-list-icon" class="custom-control-input" value="icon-list-style-4" checked="">
						<label class="custom-control-label" for="sidebariconlist-4"><i class="icon-copy dw dw-next-2"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-5" name="menu-list-icon" class="custom-control-input" value="icon-list-style-5">
						<label class="custom-control-label" for="sidebariconlist-5"><i class="dw dw-fast-forward-1"></i></label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="sidebariconlist-6" name="menu-list-icon" class="custom-control-input" value="icon-list-style-6">
						<label class="custom-control-label" for="sidebariconlist-6"><i class="dw dw-next"></i></label>
					</div>
				</div>

				<div class="reset-options pt-30 text-center">
					<button class="btn btn-danger" id="reset-settings">Reset Settings</button>
				</div>
			</div>
		</div>
	</div>

	<div class="">
		<div class="pd-ltr-20">
            <div class="card-box mb-30">
                <div class="pd-20">
                <h4 class="text-blue h4">Data Buku</h4> 
                <a href="/login" class="btn btn-primary">Login</a>          
                </div>
                
                <div class="pb-20">
                <table class="data-table table stripe hover nowrap">
                    <thead>
                    <tr>
                        <th colspan="2" class="datatable-nosort"></th>
                        <th colspan="3" class="datatable-nosort text-center">Stok</th>
                        <th class="datatable-nosort"></th>
                    </tr>
                    <tr>
                        <th class="table-plus">Nama Buku</th>
                        <th>Kategori</th>
                        <th>Total</th>
                        <th>Dipinjam</th>
                        <th>Tersedia</th>
                        <th class="datatable-nosort">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($items as $item)
                    <tr>
                        <td class="table-plus">{{ $item->name }}</td>
                        <td>
                        @foreach ($item->categories as $category)
                        <span>{{ $category->name }}</span>
                        @endforeach
                        </td>
                        <td>{{ $item->stock }}</td>
                        <td>{{ $item->borrowed_count }}</td>
                        <td>{{ $item->stock - $item->borrowed_count }}</td>
                        <td>
                        <div class="dropdown">
                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            <i class="dw dw-more"></i>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                            <a class="dropdown-item" href="{{ route('item.show', $item->id) }}"><i class="dw dw-eye"></i> View</a>
                            </div>
                        </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
	</div>
	<!-- js -->
	<script src="{{asset('deskapp2-master/vendors/scripts/core.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/script.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/process.js')}}"></script>
	<script src="{{asset('deskapp2-master/vendors/scripts/layout-settings.js')}}"></script>
	<!-- <script src="{{asset('deskapp2-master/src/plugins/apexcharts/apexcharts.min.js')}}"></script> -->
    <script src="{{asset('deskapp2-master/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>
	<!-- <script src="{{asset('deskapp2-master/vendors/scripts/dashboard.js')}}"></script> -->
    <!-- buttons for Export datatable -->
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/buttons.bootstrap4.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/buttons.flash.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/pdfmake.min.js')}}"></script>
	<script src="{{asset('deskapp2-master/src/plugins/datatables/js/vfs_fonts.js')}}"></script>
	<!-- Datatable Setting js -->
	<script src="{{asset('deskapp2-master/vendors/scripts/datatable-setting.js')}}"></script>
</body>
</html>