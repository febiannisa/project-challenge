<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'User';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.user.index')->with([
            'data' => $data,
            'users' => User::where('id', '!=', 1)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'User';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.user.create')->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required|string|min:8|same:password',
            'role' => 'in:admin,user',
        ]);

        $user = User::create(array_merge($data, [
            'password' => Hash::make($data['password']),
        ]));

        return redirect()->route('user.index')->with('success', 'User berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'User';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.user.show')->with([
            'data' => $data,
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'User';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.user.edit')->with([
            'data' => $data,
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => ['in:admin,user'],
            'age' => ['integer'],
        ]));

        $user->profile->update([
            'age' => $request->age,
        ]);

        return redirect()->route('user.index')->with('success', 'User berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->delete()) {
            return redirect()->back()->with('success', 'User berhasil dihapus');
        }

        return redirect()->back()->with('error', 'User gagal dihapus');
    }
}
