@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
	<div class="card-box pd-20 height-100-p mb-30">
		<div class="row align-items-center">
			<div class="col-md-4">
				<img src="{{asset('deskapp2-master/vendors/images/banner-img.png')}}" alt="">
			</div>
			<div class="col-md-8">
				<div class="weight-600 font-30 text-blue">Selamat Datang di SiPBar</div>
			</div>
		</div>
	</div>
@endsection